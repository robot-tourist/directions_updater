import json
import requests
import urllib3

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

import pandas as pd

from constants_loader import ACTUAL_COLS, TOKENS, CITIES
from data_loader import get_cities


def get_session() -> requests.Session:
    """Возвращает сессию, в которой отправляются запросы к API

    :return: объект сессии
    """
    session = requests.Session()
    retry = Retry(connect=3, backoff_factor=1)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def download_tickets(origin: str, destination: str) -> pd.DataFrame:
    """Парсинг данных о билетах

    :param origin: код города вылета
    :param destination: код города назначения
    :return: датафрейм с информацией о билетах
    """
    payload = {'currency': 'rub', 'period_type': 'year', 'page': 0,
               'limit': '1000', 'show_to_affiliates': 'true',
               'sorting': 'price', 'origin': origin, 'destination': destination}

    token_idx = 0
    token = TOKENS[token_idx]
    payload['token'] = token

    frame = pd.DataFrame()

    session = get_session()

    print(f'Загрузка данных о билетах из {origin} в {destination}...')

    while True:
        try:
            payload['page'] += 1
            r = session.get('http://api.travelpayouts.com/v2/prices/latest',
                            params=payload)
            json_file = json.loads(r.text)

            if not json_file.get('data'):
                break

            frame = pd.concat([frame, pd.DataFrame(json_file['data'])])
        except (OSError, requests.exceptions.ConnectionError,
                urllib3.exceptions.NewConnectionError, urllib3.exceptions.MaxRetryError):
            token_idx += 1
            token = TOKENS[token_idx % len(TOKENS)]
            payload['token'] = token

    print('Загрузка завершена. Идет обработка...')
    frame.drop_duplicates(inplace=True)

    if frame.empty:
        frame = pd.DataFrame(columns=ACTUAL_COLS)

    return frame


def get_info_by_city(origin_city_iata: str, city_data: pd.DataFrame) -> pd.DataFrame:
    """Возвращает информацию о билетах с вылетом из заданного города

    :param origin_city_iata: код города вылета
    :param city_data: датафрейм с названиями городов и их iata-кодами
    :return: датафрейм с информацией о билетах с вылетом из заданного
    города
    """
    frame = pd.DataFrame(columns=ACTUAL_COLS)

    for row in city_data.iterrows():
        dest_city_iata = row[1]['city_code']
        cur_city_tickets = download_tickets(origin_city_iata, dest_city_iata)
        frame = pd.concat([frame, cur_city_tickets])

    return frame


def get_geonames(frame: pd.DataFrame, cities_df: pd.DataFrame) -> pd.DataFrame:
    """Добавляем информацию о названиях городов и стран

    :param frame: датафрейм с информацией о билетах
    :param cities_df: датафрейм с информацией о городах и странах
    :return: датафрейм с информацией и билетах с названиями стран и городов
    """
    destination_array = pd.merge(frame, cities_df, left_on='destination',
                                 right_on='city_code', how='left')
    origin_array = pd.merge(frame, cities_df, left_on='origin',
                            right_on='city_code', how='left')

    frame['dest_country'] = destination_array['country_name']
    frame['dest_city'] = destination_array['city_name']

    frame['origin_country'] = origin_array['country_name']
    frame['origin_city'] = origin_array['city_name']

    frame.dropna(subset=['dest_country', 'dest_city', 'origin_country', 'origin_city'], inplace=True)

    return frame


def get_tickets_df() -> pd.DataFrame:
    """Возвращает информацию о билетах

    :return: датафрейм с информацией о билетах с вылетом из городов из списка
    CITIES и прибытием в города из датафреймов в папке data
    """
    directions_df = pd.DataFrame()

    cities_df = get_cities()
    for city in CITIES:
        frame = get_info_by_city(city, cities_df)
        directions_df = pd.concat((directions_df, frame))

    directions_df.reset_index(drop=True, inplace=True)
    directions_df = get_geonames(directions_df, cities_df)

    return directions_df[directions_df['actual'] == True].reset_index(drop=True)
