import datetime
import os

from constants_loader import PATH_TO_DIRECTIONS, OUTPUT_EXT
from folders import prepare_folders
from tickets_loader import get_tickets_df


def tickets_updater():
    """
    Обновление информации о билетах
    """
    print("Обновляю информацию о билетах...")
    tickets_df = get_tickets_df()
    if os.listdir(PATH_TO_DIRECTIONS):
        os.remove(os.path.join(PATH_TO_DIRECTIONS, os.listdir(PATH_TO_DIRECTIONS)[0]))
    tickets_df.to_excel(os.path.join(PATH_TO_DIRECTIONS, 'directions' + '_' + datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S") + OUTPUT_EXT))


if __name__ == '__main__':
    prepare_folders()
    tickets_updater()
