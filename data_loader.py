import os
import pandas as pd

from constants_loader import DATA_PATH


def get_cities() -> pd.DataFrame:
    """Возвращает информацию о городах

    :return: датафрейм с информацией о городах, iata-кодах и странах
    """
    cities_df = pd.DataFrame()
    for f in os.listdir(DATA_PATH):
        local_cities_df = pd.read_csv(os.path.join(DATA_PATH, f))
        cities_df = pd.concat((cities_df, local_cities_df))

    return cities_df
