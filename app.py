import datetime
import os

import pandas as pd

from flask import Flask, jsonify, make_response

from constants_loader import PATH_TO_DIRECTIONS, OUTPUT_EXT
from folders import prepare_folders
from tickets_loader import get_tickets_df

app = Flask(__name__)


@app.route('/get_tickets', methods=['GET'])
def get_tickets():
    """
    Возвращает информацию о билетах
    """
    if os.listdir(PATH_TO_DIRECTIONS):
        filename = os.listdir(PATH_TO_DIRECTIONS)[0]
        directions_df = pd.read_excel(os.path.join(PATH_TO_DIRECTIONS, filename), engine='openpyxl')
        timestamp = filename.split('_')[-1].rstrip(OUTPUT_EXT)
    else:
        directions_df = get_tickets_df()
        timestamp = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
        directions_df.to_excel(os.path.join(PATH_TO_DIRECTIONS, 'directions' + '_' + timestamp + OUTPUT_EXT))
    response = make_response(jsonify(directions_df.to_dict('records')))
    response.headers["timestamp"] = timestamp

    return response


if __name__ == '__main__':
    prepare_folders()
    app.run()
