from multiprocessing import Process

from app import app
from folders import prepare_folders
from loop import tickets_updater


if __name__ == '__main__':
    prepare_folders()
    p1 = Process(target=app.run)
    p1.start()
    p2 = Process(target=tickets_updater)
    p2.start()
