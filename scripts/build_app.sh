#!bin/bash
IMAGE_NAME=app
IMAGE_TAG=latest

APP_DIR="$(dirname -- "$(pwd)")"

DOCKER_DIR=$APP_DIR/dockerfiles/

DOCKER_FILE=DockerfileApp

ACTION=$1

if [[ $ACTION == "build" ]]; then
  docker build --rm -t $IMAGE_NAME:$IMAGE_TAG -f $DOCKER_DIR/$DOCKER_FILE $APP_DIR
fi

if [[ $ACTION == "run" ]]; then
  docker run --rm -d -p 5000:5000 --mount type=bind,source=$APP_DIR/output,target=/usr/src/dirupdater/output $IMAGE_NAME:$IMAGE_TAG
fi
