import os

from constants_loader import PATH_TO_DIRECTIONS


def prepare_folders():
    """
    Создает папки для сохранения файлов
    """
    os.makedirs(PATH_TO_DIRECTIONS, exist_ok=True)
