import os

SLEEP_TIME = 3600

DATA_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')
PATH_TO_DIRECTIONS = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'output')

ACTUAL_COLS = ['actual', 'gate', 'origin', 'destination', 'depart_date', 'return_date',
               'origin_country', 'origin_city', 'dest_country',
               'dest_city', 'value', 'duration', 'number_of_changes']

TOKENS = ['', '']

CITIES = ['MOW', 'LED']

OUTPUT_EXT = '.xlsx'
